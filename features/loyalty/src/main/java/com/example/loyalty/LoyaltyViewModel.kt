package com.example.loyalty

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.loyalty.model.CharacterList
import com.example.loyalty.model.toLocalCharacterList
import com.example.network.NetworkHelper
import com.example.network.model.ResponseState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoyaltyViewModel @Inject constructor(
    private val networkHelper: NetworkHelper
): ViewModel()
{

    fun callApi_get(){

        viewModelScope.launch(Dispatchers.IO) {
            networkHelper.GET(
                endPoint = "character",
                responseModelClass = CharacterList::class.java,
                convertFun = CharacterList::toLocalCharacterList
            ).onEach {
                when(it){
                    is ResponseState.Loading -> Log.i("TAG", "callApi_get: loading")
                    is ResponseState.Error -> Log.i("TAG", "callApi_get: error => ${it.exception?.message}")
                    is ResponseState.Success -> Log.i("TAG", "callApi_get: ok ${it.data?.results?.get(0)?.gender}")
                }
            }.catch {
                Log.i("TAG", "callApi_get: catch error")
            }.launchIn(this)
        }

    }

}