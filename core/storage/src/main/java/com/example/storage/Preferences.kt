package com.example.storage

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

internal interface DataStorePreference {

    fun <T> read(key: Preferences.Key<T>): T?
    suspend fun <T> readAsync(key: Preferences.Key<T>): T?

    fun <T> save(key: Preferences.Key<T>, value: T)
    suspend fun <T> saveAsync(key: Preferences.Key<T>, value: T)

    fun <T> remove(key: Preferences.Key<T>)

}

internal class DataStorePreferenceImpl(
    private val dataStore: DataStore<Preferences>
) : DataStorePreference {
    override fun <T> read(key: Preferences.Key<T>): T? = runBlocking {
        dataStore.data.map { it[key] }.first()
    }

    override suspend fun <T> readAsync(key: Preferences.Key<T>): T? {
        return dataStore.data.map { it[key] }.first()
    }

    override fun <T> save(key: Preferences.Key<T>, value: T): Unit = runBlocking {
        dataStore.edit {
            it[key] = value
        }
    }

    override suspend fun <T> saveAsync(key: Preferences.Key<T>, value: T) {
        dataStore.edit {
            it[key] = value
        }
    }

    override fun <T> remove(key: Preferences.Key<T>): Unit = runBlocking {
        dataStore.edit {
            it.remove(key)
        }
    }
}