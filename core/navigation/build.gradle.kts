plugins {
    id("com.android.library")
    alias(libs.plugins.kotlin.version)
}

android {
    namespace = "com.example.navigation"
    compileSdk = 33

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
}

dependencies {

    // projects
    implementation(project(":features:info"))
    implementation(project(":features:loyalty"))
    implementation(project(":features:ride"))

    api(libs.androidx.navigation.comopse)
    implementation(libs.androidx.ktx)
    implementation(libs.appcompat)
    implementation(libs.material)
    testImplementation(libs.junit)
    androidTestImplementation(libs.junitExt)
    androidTestImplementation(libs.espresso)
}