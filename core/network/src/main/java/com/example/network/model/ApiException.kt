package com.example.network.model

sealed class ApiExceptions(
    private val errorCode: Int,
    private val message: String
) {
    data class UNKNOWN_EXCEPTION(
        val errorCode: Int = 5001,
        val message: String = "خطای نامشخص"
    ) : ApiExceptions(errorCode = errorCode, message)

    data class HTTP_EXCEPTION(
        val errorCode: Int = 5002,
        val message: String = "خطای مشخص"
    ) : ApiExceptions(errorCode, message)

    data class UNKNOWN_HOST_EXCEPTION(
        val errorCode: Int = 5003,
        val message: String = "خظای سرور"
    ) : ApiExceptions(errorCode, message)

    data class IO_EXCEPTION(
        val errorCode: Int = 5004,
        val message: String = "خظای شبکه"
    ) : ApiExceptions(errorCode, message)
}
