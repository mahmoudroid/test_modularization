package com.example.network.model

data class LocalException(
    val errorCode: Int,
    val errorMessage: String
): Exception()