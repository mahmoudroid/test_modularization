package com.example.network.interfaces

import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.QueryMap
import retrofit2.http.Url

interface BaseWebservice {

    @GET
    suspend fun <T> performGetRequest(
        @Url url: String?,
        @HeaderMap headers: Map<String, String>?,
        @QueryMap queries: Map<String, String>?,
    ): Response<T>

    @POST
    suspend fun <T> performPostRequest(
        @Url url: String?,
        @HeaderMap headers: Map<String, String>?,
        @Body requestModel: RequestBody?,
        @QueryMap queries: Map<String, String>?,
    ): Response<T>

    @PUT
    suspend fun <T> performPutRequest(
        @Url url: String?,
        @HeaderMap headers: Map<String, String>?,
        @Body requestModel: RequestBody?,
        @QueryMap queries: Map<String, String>?,
    ): Response<T>

    @PATCH
    suspend fun <T> performPatchRequest(
        @Url url: String?,
        @HeaderMap headers: Map<String, String>?,
        @Body requestModel: RequestBody?,
        @QueryMap queries: Map<String, String>?,
    ): Response<T>

    @DELETE
    suspend fun <T> performDeleteRequest(
        @Url url: String?,
        @HeaderMap headers: Map<String, String>?,
        @QueryMap queries: Map<String, String>?,
    ): Response<T>

}