package com.example.storage

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject
import javax.inject.Named

class DataStoreHelper @Inject constructor(
    private val dataStore: DataStore<Preferences>,
    @Named("dataStoreCoroutineScope") private val coroutineScope: CoroutineScope
) : DataStorePreference by DataStorePreferenceImpl(dataStore) {

    fun saveBaseUrl(baseUrl: String) = save(PreferencesKeys.baseUrl, baseUrl)
    fun getBaseUrl() = read(PreferencesKeys.baseUrl)

}