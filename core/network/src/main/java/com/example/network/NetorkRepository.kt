package com.example.network

import com.example.network.interfaces.BaseWebservice
import com.example.network.interfaces.ResponseMapper
import com.example.network.model.ResponseState
import com.google.gson.Gson
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import javax.inject.Inject
import javax.inject.Named


class NetworkRepository @Inject constructor(
    private val gson: Gson,
    @Named("base_service_with_auth") private val authService: BaseWebservice,
    @Named("base_service_without_auth") private val service: BaseWebservice,
) : ResponseMapper by ResponseMapperImpl(gson) {

    companion object {
        internal val JSON: MediaType = "application/json; charset=utf-8".toMediaType()
    }

    // region GET
    suspend fun <T, R> performGetRequestWithoutAuthentication(
        url: String,
        headers: Map<String, String>,
        queryParams: Map<String, String>,
        responseModelClass: Class<T>,
        convertFun: T.() -> R
    ): Flow<ResponseState<R>> = flow {
        try {

            emit(ResponseState.Loading)

            // call api
            val result =
                service.performGetRequest<T>(url = url, headers = headers, queries = queryParams)

            // call mapper
            emit(responseMapper(result, responseModelClass, convertFun))

        } catch (exception: Exception) {
            emit(ResponseState.Error(errorMapper(exception)))
        }
    }

    suspend fun <T, R> performGetRequestWithAuthentication(
        url: String,
        headers: Map<String, String>,
        queryParams: Map<String, String>,
        responseModelClass: Class<T>,
        convertFun: T.() -> R
    ): Flow<ResponseState<R>> = flow {
        try {
            emit(ResponseState.Loading)

            // call api
            val result =
                authService.performGetRequest<T>(
                    url = url,
                    headers = headers,
                    queries = queryParams
                )

            // call mapper
            emit(responseMapper(result, responseModelClass, convertFun))

        } catch (exception: Exception) {
            emit(ResponseState.Error(errorMapper(exception)))
        }
    }

    // endregion

    // region POST
    suspend fun <T, R, E> performPostRequestWithoutAuthentication(
        url: String,
        headers: Map<String, String>,
        requestModel: E?,
        queryParams: Map<String, String>,
        responseModelClass: Class<T>,
        convertFun: T.() -> R
    ): Flow<ResponseState<R>> = flow {
        try {

            emit(ResponseState.Loading)

            // call api
            val result = service.performPostRequest<T>(
                url = url,
                headers = headers,
                requestModel = if (requestModel != null) prepareRequestBodyFromRequestModel(
                    requestModel
                ) else null,
                queries = queryParams
            )

            // call mapper
            emit(responseMapper(result, responseModelClass, convertFun))

        } catch (exception: Exception) {
            emit(ResponseState.Error(errorMapper(exception)))
        }
    }

    suspend fun <T, R, E> performPostRequestWithAuthentication(
        url: String,
        headers: Map<String, String>,
        requestModel: E?,
        queryParams: Map<String, String>,
        responseModelClass: Class<T>,
        convertFun: T.() -> R
    ): Flow<ResponseState<R>> = flow {
        try {

            emit(ResponseState.Loading)

            // call api
            val result = authService.performPostRequest<T>(
                url = url,
                headers = headers,
                requestModel = if (requestModel != null) prepareRequestBodyFromRequestModel(
                    requestModel
                ) else null,
                queries = queryParams
            )

            // call mapper
            emit(responseMapper(result, responseModelClass, convertFun))

        } catch (exception: Exception) {
            emit(ResponseState.Error(errorMapper(exception)))
        }
    }

// endregion

    // region PUT
    suspend fun <T, R, E> performPutRequestWithoutAuthentication(
        url: String,
        headers: Map<String, String>,
        requestModel: E?,
        queryParams: Map<String, String>,
        responseModelClass: Class<T>,
        convertFun: T.() -> R
    ): Flow<ResponseState<R>> = flow {
        try {

            emit(ResponseState.Loading)

            // call api
            val result = service.performPutRequest<T>(
                url = url,
                headers = headers,
                requestModel = if (requestModel != null) prepareRequestBodyFromRequestModel(
                    requestModel
                ) else null,
                queries = queryParams
            )

            // call mapper
            emit(responseMapper(result, responseModelClass, convertFun))

        } catch (exception: Exception) {
            emit(ResponseState.Error(errorMapper(exception)))
        }
    }

    suspend fun <T, R, E> performPutRequestWithAuthentication(
        url: String,
        headers: Map<String, String>,
        requestModel: E?,
        queryParams: Map<String, String>,
        responseModelClass: Class<T>,
        convertFun: T.() -> R
    ): Flow<ResponseState<R>> = flow {
        try {

            emit(ResponseState.Loading)

            // call api
            val result = authService.performPutRequest<T>(
                url = url,
                headers = headers,
                requestModel = if (requestModel != null) prepareRequestBodyFromRequestModel(
                    requestModel
                ) else null,
                queries = queryParams
            )

            // call mapper
            emit(responseMapper(result, responseModelClass, convertFun))

        } catch (exception: Exception) {
            emit(ResponseState.Error(errorMapper(exception)))
        }
    }

// endregion

    // region PATCH
    suspend fun <T, R, E> performPatchRequestWithoutAuthentication(
        url: String,
        headers: Map<String, String>,
        requestModel: E?,
        queryParams: Map<String, String>,
        responseModelClass: Class<T>,
        convertFun: T.() -> R
    ): Flow<ResponseState<R>> = flow {
        try {

            emit(ResponseState.Loading)

            // call api
            val result = service.performPatchRequest<T>(
                url = url,
                headers = headers,
                requestModel = if (requestModel != null) prepareRequestBodyFromRequestModel(
                    requestModel
                ) else null,
                queries = queryParams
            )

            // call mapper
            emit(responseMapper(result, responseModelClass, convertFun))

        } catch (exception: Exception) {
            emit(ResponseState.Error(errorMapper(exception)))
        }
    }

    suspend fun <T, R, E> performPatchRequestWithAuthentication(
        url: String,
        headers: Map<String, String>,
        requestModel: E?,
        queryParams: Map<String, String>,
        responseModelClass: Class<T>,
        convertFun: T.() -> R
    ): Flow<ResponseState<R>> = flow {
        try {

            emit(ResponseState.Loading)

            // call api
            val result = authService.performPatchRequest<T>(
                url = url,
                headers = headers,
                requestModel = if (requestModel != null) prepareRequestBodyFromRequestModel(
                    requestModel
                ) else null,
                queries = queryParams
            )

            // call mapper
            emit(responseMapper(result, responseModelClass, convertFun))

        } catch (exception: Exception) {
            emit(ResponseState.Error(errorMapper(exception)))
        }
    }

// endregion

    // region DELETE
    suspend fun <T, R> performDeleteRequestWithoutAuthentication(
        url: String,
        headers: Map<String, String>,
        queryParams: Map<String, String>,
        responseModelClass: Class<T>,
        convertFun: T.() -> R
    ): Flow<ResponseState<R>> = flow {
        try {

            emit(ResponseState.Loading)

            // call api
            val result = service.performDeleteRequest<T>(
                url = url,
                headers = headers,
                queries = queryParams
            )

            // call mapper
            emit(responseMapper(result, responseModelClass, convertFun))

        } catch (exception: Exception) {
            emit(ResponseState.Error(errorMapper(exception)))
        }
    }

    suspend fun <T, R> performDeleteRequestWithAuthentication(
        url: String,
        headers: Map<String, String>,
        queryParams: Map<String, String>,
        responseModelClass: Class<T>,
        convertFun: T.() -> R
    ): Flow<ResponseState<R>> = flow {
        try {

            emit(ResponseState.Loading)

            // call api
            val result = authService.performDeleteRequest<T>(
                url = url,
                headers = headers,
                queries = queryParams
            )

            // call mapper
            emit(responseMapper(result, responseModelClass, convertFun))

        } catch (exception: Exception) {
            emit(ResponseState.Error(errorMapper(exception)))
        }
    }

// endregion




    // region helper functions
    private fun <T> prepareRequestBodyFromRequestModel(postBody: T): RequestBody {
        postBody.let {
            val requestBodyString: String = gson.toJson(it)
            return requestBodyString.toRequestBody(JSON)
        }
    }
    // endregion


}