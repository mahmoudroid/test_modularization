package com.example.network

import javax.inject.Inject

class NetworkHelper @Inject constructor(
    private val repository: NetworkRepository
) {

    private val baseUrl = "https://rickandmortyapi.com/api/" // move it to gradle
    private fun getApiUrl(endPoint: String) = baseUrl + endPoint

    suspend fun <T, R> GET(
        endPoint: String,
        headers: Map<String, String> = mutableMapOf(),
        queryParams: Map<String, String> = mutableMapOf(),
        responseModelClass: Class<T>,
        convertFun: T.() -> R,
        hasAuthentication: Boolean = false
    ) = when (hasAuthentication) {
        true ->
            repository.performGetRequestWithAuthentication(
                getApiUrl(endPoint),
                headers,
                queryParams,
                responseModelClass,
                convertFun
            )

        false ->
            repository.performGetRequestWithoutAuthentication(
                getApiUrl(endPoint),
                headers,
                queryParams,
                responseModelClass,
                convertFun
            )
    }

    suspend fun <T, R, E> POST(
        endPoint: String,
        headers: Map<String, String> = mutableMapOf(),
        requestModel: E?,
        queryParams: Map<String, String> = mutableMapOf(),
        responseModelClass: Class<T>,
        convertFun: T.() -> R,
        hasAuthentication: Boolean = false
    ) = when (hasAuthentication) {
        true ->
            repository.performPostRequestWithAuthentication(
                getApiUrl(endPoint),
                headers,
                requestModel,
                queryParams,
                responseModelClass,
                convertFun
            )

        false ->
            repository.performPostRequestWithoutAuthentication(
                getApiUrl(endPoint),
                headers,
                requestModel,
                queryParams,
                responseModelClass,
                convertFun
            )
    }

    suspend fun <T, R, E> PUT(
        endPoint: String,
        headers: Map<String, String> = mutableMapOf(),
        requestModel: E?,
        queryParams: Map<String, String> = mutableMapOf(),
        responseModelClass: Class<T>,
        convertFun: T.() -> R,
        hasAuthentication: Boolean = false
    ) = when (hasAuthentication) {
        true ->
            repository.performPutRequestWithAuthentication(
                getApiUrl(endPoint),
                headers,
                requestModel,
                queryParams,
                responseModelClass,
                convertFun
            )

        false ->
            repository.performPutRequestWithoutAuthentication(
                getApiUrl(endPoint),
                headers,
                requestModel,
                queryParams,
                responseModelClass,
                convertFun
            )
    }

    suspend fun <T, R, E> PATCH(
        endPoint: String,
        headers: Map<String, String> = mutableMapOf(),
        requestModel: E?,
        queryParams: Map<String, String> = mutableMapOf(),
        responseModelClass: Class<T>,
        convertFun: T.() -> R,
        hasAuthentication: Boolean = false
    ) = when (hasAuthentication) {
        true ->
            repository.performPatchRequestWithAuthentication(
                getApiUrl(endPoint),
                headers,
                requestModel,
                queryParams,
                responseModelClass,
                convertFun
            )

        false ->
            repository.performPatchRequestWithoutAuthentication(
                getApiUrl(endPoint),
                headers,
                requestModel,
                queryParams,
                responseModelClass,
                convertFun
            )
    }

    suspend fun <T, R> DELETE(
        endPoint: String,
        headers: Map<String, String> = mutableMapOf(),
        queryParams: Map<String, String> = mutableMapOf(),
        responseModelClass: Class<T>,
        convertFun: T.() -> R,
        hasAuthentication: Boolean = false
    ) = when (hasAuthentication) {
        true ->
            repository.performDeleteRequestWithAuthentication(
                getApiUrl(endPoint),
                headers,
                queryParams,
                responseModelClass,
                convertFun
            )

        false ->
            repository.performDeleteRequestWithoutAuthentication(
                getApiUrl(endPoint),
                headers,
                queryParams,
                responseModelClass,
                convertFun
            )
    }

}