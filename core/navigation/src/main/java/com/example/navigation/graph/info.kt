package com.example.navigation.graph

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.example.info.InfoRoute
import com.example.navigation.Destinations

fun NavGraphBuilder.info(navController: NavController){
    composable(
        route = Destinations.InfoScreen().route,
    ){
        val age: Int = it.arguments?.getInt(Destinations.InfoScreen().age) ?: -1
        InfoRoute(
            age = age,
            backCallBack = {
                navController.navigateUp()
            }
        )
    }
}