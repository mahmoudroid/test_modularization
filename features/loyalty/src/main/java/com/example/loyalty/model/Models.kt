package com.example.loyalty.model

data class CharacterList(
    val results: List<Character>
)

data class Character(
    val created: String,
    val gender: String,
    val id: Int,
    val image: String,
    val name: String,
    val species: String,
    val status: String,
    val type: String,
    val url: String
)

//////////////////////// local

data class LocalCharacterList(
    val results: List<LocalCharacter>
)

data class LocalCharacter(
    val created: String,
    val gender: String,
)

// extensions

fun Character.toLocalCharacter() = LocalCharacter(
    this.created,
    this.gender
)

fun CharacterList.toLocalCharacterList(): LocalCharacterList{
    val list = mutableListOf<LocalCharacter>()

    for (item in this.results){
        list.add(item.toLocalCharacter())
    }
    return LocalCharacterList(list)
}