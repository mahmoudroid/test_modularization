package com.example.navigation.graph

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.example.navigation.Destinations
import com.example.ride.RideRouter

fun NavGraphBuilder.ride(navController: NavController){
   composable(Destinations.RideScreen.route){
       RideRouter()
   }
}