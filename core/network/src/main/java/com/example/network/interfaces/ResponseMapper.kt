package com.example.network.interfaces

import com.example.network.model.ErrorResponse
import com.example.network.model.LocalException
import com.example.network.model.ResponseState
import retrofit2.Response
import java.lang.Exception

interface ResponseMapper {
    fun <T, R> responseMapper(
        response: Response<T>,
        responseModelClass: Class<T>,
        convertFun: T.() -> R
    ): ResponseState<R>

    fun errorMapper(
        exception: Exception,
        httpErrorCode: ErrorResponse? = null,
        errorCode: Int? = null,
    ): LocalException
}