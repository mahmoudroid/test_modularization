package com.example.network

import com.example.network.interfaces.ResponseMapper
import com.example.network.model.ApiExceptions
import com.example.network.model.ErrorResponse
import com.example.network.model.LocalException
import com.example.network.model.ResponseState
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException
import javax.inject.Inject
import kotlin.Exception

class ResponseMapperImpl @Inject constructor(
    private val gson: Gson
) : ResponseMapper {
    override fun <T, R> responseMapper(
        response: Response<T>,
        responseModelClass: Class<T>,
        convertFun: T.() -> R
    ): ResponseState<R> {

        if (response.isSuccessful) {

            // convert result to response model
            val responseModel =
                gson.fromJson(gson.toJsonTree(response.body()).asJsonObject, responseModelClass)

            // invoke response to local model function
            val localResult: R = convertFun(responseModel)

            return ResponseState.Success(localResult)

        } else {

            return ResponseState.Error(
                errorMapper(
                    HttpException(response),
                    getResponseException(response),
                    response.code()
                )
            )

        }
    }

    override fun errorMapper(
        exception: Exception,
        httpErrorCode: ErrorResponse?,
        errorCode: Int?,
    ): LocalException {
        return when (exception) {
            is HttpException -> {

                httpErrorCode?.let {
                    return if (httpErrorCode.code in 500..599)
                        LocalException(
                            ApiExceptions.HTTP_EXCEPTION().errorCode,
                            "مشکلی پیش آمده است. لطفا دقایقی دیگر تلاش نمایید"
                        )
                    else
                        LocalException(
                            errorCode ?: ApiExceptions.UNKNOWN_EXCEPTION().errorCode,
                            "خطای خاص که متن فارسیش مشخص باید باشه" // todo: get this message from persian error messages
                        )

                } ?: kotlin.run {
                    return ApiExceptions.UNKNOWN_EXCEPTION().let {
                        LocalException(it.errorCode, it.message)
                    }
                }

            }

            is UnknownHostException -> ApiExceptions.UNKNOWN_HOST_EXCEPTION().let {
                LocalException(it.errorCode, it.message)
            }

            is IOException -> ApiExceptions.IO_EXCEPTION().let {
                LocalException(it.errorCode, it.message)
            }

            else -> ApiExceptions.UNKNOWN_EXCEPTION().let {
                LocalException(it.errorCode, it.message)
            }
        }
    }

    private fun <T> getResponseException(response: Response<T>): ErrorResponse? {
        val type = object : TypeToken<ErrorResponse>() {}.type
        return gson.fromJson(response.errorBody()?.charStream(), type)
    }

}