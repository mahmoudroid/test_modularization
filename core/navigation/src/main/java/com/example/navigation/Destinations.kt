package com.example.navigation

sealed class Destinations(val route: String){

    object LoyaltyScreen: Destinations("loyalty_screen")
    object RideScreen: Destinations("ride_screen")
    data class InfoScreen(val age: String = "age") : Destinations("info_screen")

}