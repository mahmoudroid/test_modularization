package com.example.info

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel

@Composable
fun InfoRoute(
    infoViewModel: InfoViewModel = hiltViewModel(),
    age: Int,
    backCallBack: () -> Unit
) {


    InfoScreen(age,backCallBack = backCallBack)

}

@Composable
fun InfoScreen(
    age: Int,
    backCallBack: () -> Unit
) {

    Surface(
        modifier = Modifier.fillMaxSize(),
        color = Color.LightGray
    ) {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Button(onClick = { backCallBack.invoke() }) {
                Text(text = "Go Back")
            }
            Text(text = "text is $age")
        }
    }
}

@Preview
@Composable
fun InfoScreenPreview() {
    InfoScreen(age = 14) {
        
    }
}
