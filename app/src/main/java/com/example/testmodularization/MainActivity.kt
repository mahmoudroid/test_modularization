package com.example.testmodularization

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.navigation.ComposeNewsNavHost
import com.example.navigation.Destinations
import com.example.sharedcomponent.navigation.BottomNavItem
import com.example.testmodularization.component.BottomNavigationBar
import com.example.uikit.theme.TestModularizationTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val items = listOf(
        BottomNavItem(
            name = "Markets",
            route = Destinations.LoyaltyScreen.route,
            icon = Icons.Default.Home
        ),
        BottomNavItem(
            name = "Favorite",
            route = Destinations.RideScreen.route,
            icon = Icons.Default.Favorite,
        ),
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TestModularizationTheme {

                val navController = rememberNavController()
                val backStackEntry = navController.currentBackStackEntryAsState()
                val currentScreenRoute = backStackEntry.value?.destination?.route
                val bottomNavVisible = items.any {
                    it.route == currentScreenRoute
                }

                Scaffold(
                    bottomBar = {
                        AnimatedVisibility(
                            visible = bottomNavVisible,
                            enter = slideInVertically { it },
                            exit = slideOutVertically { it },
                        ) {

                            BottomNavigationBar(
                                items = items,
                                currentScreenRoute = currentScreenRoute
                            ) {
                                navController.navigate(it.route)
                            }
                        }
                    }
                ) {
                    ComposeNewsNavHost(
                        navController = navController,
                        modifier = Modifier.padding(bottom = it.calculateBottomPadding())
                    )
                }

            }
        }
    }
}
