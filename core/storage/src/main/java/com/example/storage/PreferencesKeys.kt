package com.example.storage

import androidx.datastore.preferences.core.stringPreferencesKey

internal object PreferencesKeys {
    val baseUrl = stringPreferencesKey("baseUrl")
}