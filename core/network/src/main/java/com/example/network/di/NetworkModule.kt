package com.example.network.di


import com.example.network.interfaces.BaseWebservice
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().serializeNulls().setLenient().create()
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient{
        return OkHttpClient.Builder()
            .readTimeout(30,TimeUnit.SECONDS)
            .writeTimeout(30,TimeUnit.SECONDS)
            .connectTimeout(30,TimeUnit.SECONDS)
            .followSslRedirects(false)
            .retryOnConnectionFailure(true)
            .hostnameVerifier { hostname, session -> true }
            .build()
    }

    // region retrofit without auth

    @Named("retrofit_without_auth")
    @Singleton
    @Provides
    fun provideRetrofitWithoutAuth(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://rickandmortyapi.com")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Named("base_service_without_auth")
    @Singleton
    @Provides
    fun provideWebServiceWithoutAuth(@Named("retrofit_without_auth") retrofit: Retrofit): BaseWebservice{
        return retrofit.create(BaseWebservice::class.java)
    }

    // endregion

    // region retrofit with auth

    @Named("retrofit_with_auth")
    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://rickandmortyapi.com/api/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Named("base_service_with_auth")
    @Singleton
    @Provides
    fun provideWebService(@Named("retrofit_without_auth") retrofit: Retrofit): BaseWebservice{
        return retrofit.create(BaseWebservice::class.java)
    }

    // endregion
}