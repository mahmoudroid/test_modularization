package com.example.loyalty

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.hilt.navigation.compose.hiltViewModel


@Composable
fun LoyaltyRoute(
    viewModel: LoyaltyViewModel = hiltViewModel(),
    onNextPageCallback: (value: Int) -> Unit
) {

    LaunchedEffect(key1 = Unit){
        //viewModel.callApi_zeno()
       // viewModel.callApi_unknown()
        viewModel.callApi_get()
    }

    LoyaltyScreen(onNextPageCallback = onNextPageCallback)

}

@Composable
fun LoyaltyScreen(
    onNextPageCallback: (value: Int) -> Unit
) {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = Color.Green
    ) {
        Column {
            Button(onClick = { onNextPageCallback.invoke(7) }) {
                Text(text = "go next screen")
            }
        }
    }
}