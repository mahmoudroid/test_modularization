pluginManagement {
    includeBuild("plugins")
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "TestModularization"
include(":app")
include(":core")
include(":features:ride")
include(":core:uikit")
include(":core:navigation")
include(":features:loyalty")
include(":features:info")
include(":core:sharedComponent")
include(":core:network")
include(":core:storage")
