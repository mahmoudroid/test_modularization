package com.example.navigation

import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.loyalty.LoyaltyScreen
import com.example.navigation.graph.info
import com.example.navigation.graph.loyalty
import com.example.navigation.graph.ride
import com.example.ride.RideScreen

@Composable
fun ComposeNewsNavHost(
    navController: NavHostController,
    modifier: Modifier,
) {
    NavHost(
        navController = navController,
        startDestination = Destinations.LoyaltyScreen.route,
        modifier = modifier,
        enterTransition = { EnterTransition.None },
        exitTransition = { ExitTransition.None },
    ) {
        loyalty(navController)
        ride(navController)
        info(navController)
    }
}