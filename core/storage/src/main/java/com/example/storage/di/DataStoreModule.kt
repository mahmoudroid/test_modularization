package com.example.storage.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.core.handlers.ReplaceFileCorruptionHandler
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.preferencesDataStoreFile
import com.example.storage.DataStoreHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataStoreModule {

    @Singleton
    @Provides
    @Named("dataStoreCoroutineScope")
    fun provideDataStoreScope(): CoroutineScope = CoroutineScope(Dispatchers.IO + SupervisorJob())

    @Singleton
    @Provides
    fun providePreferencesDatastore(
        @ApplicationContext appContext: Context,
        @Named("dataStoreCoroutineScope") coroutineScope: CoroutineScope
    ): DataStore<Preferences> = PreferenceDataStoreFactory.create(
        corruptionHandler = ReplaceFileCorruptionHandler(
            produceNewData = { emptyPreferences() }
        ),
        produceFile = { appContext.preferencesDataStoreFile("cypherland_data_store") },
        scope = coroutineScope,
    )

    @Singleton
    @Provides
    fun provideDataStoreHelper(
        dataStore: DataStore<Preferences>,
        @Named("dataStoreCoroutineScope") coroutineScope: CoroutineScope
    ): DataStoreHelper = DataStoreHelper(dataStore, coroutineScope)

}