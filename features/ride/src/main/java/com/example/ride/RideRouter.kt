package com.example.ride

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController

@Composable
fun RideRouter(
    rideViewModel: RideViewModel = hiltViewModel()
) {
    RideScreen()
}

@Composable
fun RideScreen() {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = Color.Cyan
    ) {

    }
}