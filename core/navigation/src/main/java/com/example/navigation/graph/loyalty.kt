package com.example.navigation.graph

import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.example.loyalty.LoyaltyRoute
import com.example.navigation.Destinations
import com.example.navigation.extension.navigate

fun NavGraphBuilder.loyalty(
    navController: NavController,
) {
    composable(Destinations.LoyaltyScreen.route) {
        LoyaltyRoute(
            onNextPageCallback = { intValue ->
                // go to info route
                navController.navigate(
                    route = Destinations.InfoScreen().route,
                    args = bundleOf(
                        Destinations.InfoScreen().age to intValue
                    )
                )
            }
        )
    }
}