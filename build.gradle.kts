// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    libs.plugins.apply {
        alias(android.application) apply false
        alias(kotlin.version) apply false
        alias(kapt) apply false
        alias(hilt.android) apply false
    }
}